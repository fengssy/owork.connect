var testMessageTypeCodes = {
    Login : 10001,
    LoginResult : 10002,
    CodeToName : {  "Op_10001": "Login" , "Op_10002": "LoginResult"  }
}

if(typeof module!=="undefined"){
    module.exports=testMessageTypeCodes;
}

