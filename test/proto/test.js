/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
(function(global, factory) { /* global define, require, module */

    /* AMD */ if (typeof define === 'function' && define.amd)
        define(["protobufjs/minimal"], factory);

    /* CommonJS */ else if (typeof require === 'function' && typeof module === 'object' && module && module.exports)
        module.exports = factory(require("protobufjs/minimal"));

})(this, function($protobuf) {
    "use strict";

    // Common aliases
    var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;
    
    // Exported root namespace
    var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});
    
    $root.Test = (function() {
    
        /**
         * Namespace Test.
         * @exports Test
         * @namespace
         */
        var Test = {};
    
        Test.Project1 = (function() {
    
            /**
             * Namespace Project1.
             * @memberof Test
             * @namespace
             */
            var Project1 = {};
    
            Project1.Login = (function() {
    
                /**
                 * Properties of a Login.
                 * @memberof Test.Project1
                 * @interface ILogin
                 * @property {number|Long|null} [RpcId] Login RpcId
                 * @property {string|null} [AuCode] Login AuCode
                 */
    
                /**
                 * Constructs a new Login.
                 * @memberof Test.Project1
                 * @classdesc Represents a Login.
                 * @implements ILogin
                 * @constructor
                 * @param {Test.Project1.ILogin=} [properties] Properties to set
                 */
                function Login(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * Login RpcId.
                 * @member {number|Long} RpcId
                 * @memberof Test.Project1.Login
                 * @instance
                 */
                Login.prototype.RpcId = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    
                /**
                 * Login AuCode.
                 * @member {string} AuCode
                 * @memberof Test.Project1.Login
                 * @instance
                 */
                Login.prototype.AuCode = "";
    
                /**
                 * Encodes the specified Login message. Does not implicitly {@link Test.Project1.Login.verify|verify} messages.
                 * @function encode
                 * @memberof Test.Project1.Login
                 * @static
                 * @param {Test.Project1.ILogin} message Login message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Login.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.AuCode != null && Object.hasOwnProperty.call(message, "AuCode"))
                        writer.uint32(/* id 1, wireType 2 =*/10).string(message.AuCode);
                    if (message.RpcId != null && Object.hasOwnProperty.call(message, "RpcId"))
                        writer.uint32(/* id 90, wireType 0 =*/720).int64(message.RpcId);
                    return writer;
                };
    
                /**
                 * Encodes the specified Login message, length delimited. Does not implicitly {@link Test.Project1.Login.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof Test.Project1.Login
                 * @static
                 * @param {Test.Project1.ILogin} message Login message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Login.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a Login message from the specified reader or buffer.
                 * @function decode
                 * @memberof Test.Project1.Login
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {Test.Project1.Login} Login
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Login.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Test.Project1.Login();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 90:
                            message.RpcId = reader.int64();
                            break;
                        case 1:
                            message.AuCode = reader.string();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a Login message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof Test.Project1.Login
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {Test.Project1.Login} Login
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Login.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                return Login;
            })();
    
            Project1.LoginResult = (function() {
    
                /**
                 * Properties of a LoginResult.
                 * @memberof Test.Project1
                 * @interface ILoginResult
                 * @property {number|Long|null} [RpcId] LoginResult RpcId
                 * @property {number|null} [Error] LoginResult Error
                 * @property {string|null} [Message] LoginResult Message
                 * @property {string|null} [Token] LoginResult Token
                 */
    
                /**
                 * Constructs a new LoginResult.
                 * @memberof Test.Project1
                 * @classdesc Represents a LoginResult.
                 * @implements ILoginResult
                 * @constructor
                 * @param {Test.Project1.ILoginResult=} [properties] Properties to set
                 */
                function LoginResult(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * LoginResult RpcId.
                 * @member {number|Long} RpcId
                 * @memberof Test.Project1.LoginResult
                 * @instance
                 */
                LoginResult.prototype.RpcId = $util.Long ? $util.Long.fromBits(0,0,false) : 0;
    
                /**
                 * LoginResult Error.
                 * @member {number} Error
                 * @memberof Test.Project1.LoginResult
                 * @instance
                 */
                LoginResult.prototype.Error = 0;
    
                /**
                 * LoginResult Message.
                 * @member {string} Message
                 * @memberof Test.Project1.LoginResult
                 * @instance
                 */
                LoginResult.prototype.Message = "";
    
                /**
                 * LoginResult Token.
                 * @member {string} Token
                 * @memberof Test.Project1.LoginResult
                 * @instance
                 */
                LoginResult.prototype.Token = "";
    
                /**
                 * Encodes the specified LoginResult message. Does not implicitly {@link Test.Project1.LoginResult.verify|verify} messages.
                 * @function encode
                 * @memberof Test.Project1.LoginResult
                 * @static
                 * @param {Test.Project1.ILoginResult} message LoginResult message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                LoginResult.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.Token != null && Object.hasOwnProperty.call(message, "Token"))
                        writer.uint32(/* id 10, wireType 2 =*/82).string(message.Token);
                    if (message.RpcId != null && Object.hasOwnProperty.call(message, "RpcId"))
                        writer.uint32(/* id 90, wireType 0 =*/720).int64(message.RpcId);
                    if (message.Error != null && Object.hasOwnProperty.call(message, "Error"))
                        writer.uint32(/* id 91, wireType 0 =*/728).int32(message.Error);
                    if (message.Message != null && Object.hasOwnProperty.call(message, "Message"))
                        writer.uint32(/* id 92, wireType 2 =*/738).string(message.Message);
                    return writer;
                };
    
                /**
                 * Encodes the specified LoginResult message, length delimited. Does not implicitly {@link Test.Project1.LoginResult.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof Test.Project1.LoginResult
                 * @static
                 * @param {Test.Project1.ILoginResult} message LoginResult message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                LoginResult.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes a LoginResult message from the specified reader or buffer.
                 * @function decode
                 * @memberof Test.Project1.LoginResult
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {Test.Project1.LoginResult} LoginResult
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                LoginResult.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Test.Project1.LoginResult();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 90:
                            message.RpcId = reader.int64();
                            break;
                        case 91:
                            message.Error = reader.int32();
                            break;
                        case 92:
                            message.Message = reader.string();
                            break;
                        case 10:
                            message.Token = reader.string();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes a LoginResult message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof Test.Project1.LoginResult
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {Test.Project1.LoginResult} LoginResult
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                LoginResult.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                return LoginResult;
            })();
    
            return Project1;
        })();
    
        return Test;
    })();
    
    $root.google = (function() {
    
        /**
         * Namespace google.
         * @exports google
         * @namespace
         */
        var google = {};
    
        google.protobuf = (function() {
    
            /**
             * Namespace protobuf.
             * @memberof google
             * @namespace
             */
            var protobuf = {};
    
            protobuf.Any = (function() {
    
                /**
                 * Properties of an Any.
                 * @memberof google.protobuf
                 * @interface IAny
                 * @property {string|null} [typeUrl] Any typeUrl
                 * @property {Uint8Array|null} [value] Any value
                 */
    
                /**
                 * Constructs a new Any.
                 * @memberof google.protobuf
                 * @classdesc Represents an Any.
                 * @implements IAny
                 * @constructor
                 * @param {google.protobuf.IAny=} [properties] Properties to set
                 */
                function Any(properties) {
                    if (properties)
                        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                            if (properties[keys[i]] != null)
                                this[keys[i]] = properties[keys[i]];
                }
    
                /**
                 * Any typeUrl.
                 * @member {string} typeUrl
                 * @memberof google.protobuf.Any
                 * @instance
                 */
                Any.prototype.typeUrl = "";
    
                /**
                 * Any value.
                 * @member {Uint8Array} value
                 * @memberof google.protobuf.Any
                 * @instance
                 */
                Any.prototype.value = $util.newBuffer([]);
    
                /**
                 * Encodes the specified Any message. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
                 * @function encode
                 * @memberof google.protobuf.Any
                 * @static
                 * @param {google.protobuf.IAny} message Any message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Any.encode = function encode(message, writer) {
                    if (!writer)
                        writer = $Writer.create();
                    if (message.typeUrl != null && Object.hasOwnProperty.call(message, "typeUrl"))
                        writer.uint32(/* id 1, wireType 2 =*/10).string(message.typeUrl);
                    if (message.value != null && Object.hasOwnProperty.call(message, "value"))
                        writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.value);
                    return writer;
                };
    
                /**
                 * Encodes the specified Any message, length delimited. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
                 * @function encodeDelimited
                 * @memberof google.protobuf.Any
                 * @static
                 * @param {google.protobuf.IAny} message Any message or plain object to encode
                 * @param {$protobuf.Writer} [writer] Writer to encode to
                 * @returns {$protobuf.Writer} Writer
                 */
                Any.encodeDelimited = function encodeDelimited(message, writer) {
                    return this.encode(message, writer).ldelim();
                };
    
                /**
                 * Decodes an Any message from the specified reader or buffer.
                 * @function decode
                 * @memberof google.protobuf.Any
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @param {number} [length] Message length if known beforehand
                 * @returns {google.protobuf.Any} Any
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Any.decode = function decode(reader, length) {
                    if (!(reader instanceof $Reader))
                        reader = $Reader.create(reader);
                    var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.Any();
                    while (reader.pos < end) {
                        var tag = reader.uint32();
                        switch (tag >>> 3) {
                        case 1:
                            message.typeUrl = reader.string();
                            break;
                        case 2:
                            message.value = reader.bytes();
                            break;
                        default:
                            reader.skipType(tag & 7);
                            break;
                        }
                    }
                    return message;
                };
    
                /**
                 * Decodes an Any message from the specified reader or buffer, length delimited.
                 * @function decodeDelimited
                 * @memberof google.protobuf.Any
                 * @static
                 * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
                 * @returns {google.protobuf.Any} Any
                 * @throws {Error} If the payload is not a reader or valid buffer
                 * @throws {$protobuf.util.ProtocolError} If required fields are missing
                 */
                Any.decodeDelimited = function decodeDelimited(reader) {
                    if (!(reader instanceof $Reader))
                        reader = new $Reader(reader);
                    return this.decode(reader, reader.uint32());
                };
    
                return Any;
            })();
    
            return protobuf;
        })();
    
        return google;
    })();

    return $root;
});
