require('../external/external.nodejs.ZWebSocket.js');
require('../test/proto/test');
var testMessageTypeCodes = require('../test/proto/testMessageTypeCodes');

const ProtobufHelper = require('../dist/src/ProtobufHelper').ProtobufHelper;
const ClientFactory = require('../dist/src/ClientFactory').ClientFactory;


var protobufHelper = new ProtobufHelper("Test.Project1",
    (op) => testMessageTypeCodes.CodeToName["Op_" + op]);
var client = ClientFactory.buildWebSocketClient(protobufHelper, "ws://127.0.0.1:11101/");

console.log("开始连接");
client.connect(function (errMsg, authCallback) {
    if (errMsg) {
        console.error("连接失败:" + errMsg);
        return;
    }
    console.log("成功连上服务器,开始认证");
    client.sendRpc(testMessageTypeCodes.Login, { AuCode: "f1c03daa663f4dcdae695cb64428696d" }, testMessageTypeCodes.LoginResult,
        function (connectErr, reOpCode, msg) {
            if (connectErr) {
                console.error("认证失败:" + connectErr);
                return authCallback(false);
            }
            if (msg.Error) {
                console.error("认证失败:" + msg.Message);
                return authCallback(false);
            }

            console.log("认证成功!");
            authCallback(true);
        }
    );
});
