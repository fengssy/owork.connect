# owork.connect

## 介绍
基于[数据传输协议]封装的客户端连接工具, 数据采用protobuf序列化, 内置websocket实现.

### 数据传输协议

定义|说明
:-:|:-
传输方式|使用二进制传输
一个数据包|消息类型(ushort,2个字节)<br>+后面消息体长度(int,4个字节)<br>+消息体二进制数据
消息类型|为ushort类型,采用低位在前的方式转为byte[]<br>比如消息类型ushort值为10001,<br>二进制为00100111 00010001<br>存储为byte[]={00010001, 00100111}<br>即为:{17, 39}
消息体长度|为int类型,转换方式同消息类型


## 安装

npm i --save owork.connect


## 依赖
除了npm包依赖protobufjs之外, 还依赖外部实现:

./define/external.*.d.ts 需要外部实现的功能定义在这里了, 为了排除环境因素的设计

比如抽出ZWebsocket的定义,使用时可以根据不同的环境进行不同的定义,比如微信小程序,可以用微信的websocket类进行实现,H5就用原生的实现

./external/external.h5.* .js 为h5环境下的一套实现,可以参考使用
./external/external.node.* .js 为h5环境下的一套实现,可以参考使用


## 使用

### Client
#### 使用工厂来创建不同实现的客户端:
##### websocket作为客户端:
```javascript
ClientFactory.buildWebSocketClient()
```

### ProtobufHelper
**封装数据传输协议的序列化/反序列化操作, 以及一些拓展功能**  

#### 1. pbjs: npm包,用来生成对应的proto类文件,自行百度资料  
#### 2. 消息类型码映射为proto类  
##### 2.1. 可自行实现类型码到proto类型的映射关系  
```typescript
var protobufHelper = new ProtobufHelper(null, "proto里的包名",
    (op) => 类型码op对应proto里的类, window或者global);

```
##### 2.2 也可用 [owork.connect.buildproto](https://www.npmjs.com/package/owork.connect.buildproto) 生成类型码的TS/JS类文件(仅支持windows)  



### websocket例子:  
```typescript
./test/websocket.js
```

### 搭配proto文件使用:  

#### proto生成静态js的方式(推荐)
用参数: -t static-module -w commonjs
```bat
pbjs --no-create --no-verify --no-convert -t static-module -w commonjs -o ./输出.js ./输入.proto
```

nodejs环境的使用方式:
```typescript
import root from "./输出.js"

var protobufHelper = new ProtobufHelper(root, "proto里的命名空间" , 
    (op) => MessageTypeCodes.CodeToName["Op_" + op], globalThis);
```

web环境的使用方式:
```html
<script src="protobuf.min.js"></script>
<script src="输出.js"></script>
<script src="ProtobufHelper.js"></script>
<script>
//web的环境会自动注册到全局,所以第一个参数传null,ProtobufHelper会自动去识别
var protobufHelper = new ProtobufHelper(null, "proto里的命名空间" , 
    (op) => MessageTypeCodes.CodeToName["Op_" + op], globalThis);
</script>
```

#### 动态解析proto,或者json解析的方式
动态解析:  
```javascript
protobuf.load("输入.proto", function(err, root) {
    //用root传入ProtobufHelper构造的第一个参数
});
```
json解析方式1:  
```javascript
protobuf.load("输入.json", function(err, root) {
    //用root传入ProtobufHelper构造的第一个参数
});
```
json解析方式2:  
```javascript
var jsonDescriptor = require("./输入.json");
//需要开启resolveJsonModule
//或者将pbjs生成的json内容加个前缀:"export const json=",然后生成的json改成js,采用模块化方式加载这个json对象
var root = protobuf.Root.fromJSON(jsonDescriptor);
//同样用这个root传入ProtobufHelper构造的第一个参数
```

更多请参考protobufjs官方文档: https://github.com/protobufjs/protobuf.js


## 更新日志
+ V0.0.7 默认改用编译好的js,同时提供各模块的声明文件
+ V0.0.6 ProtobufHelper全局注册使用globalThis作为默认值,补充sendRpc中的RpcId可用Long类型,将root开放传参,适用于多种环境
+ V0.0.5 补充源码地址  
+ V0.0.4 分离命令行工具,减少依赖  
+ V0.0.3 命令行工具升级,生成消息类型码js文件时会判断导出  
+ V0.0.2 完成代码结构的重构,具备直接使用的能力,并包含单元测试(使用示例)  
+ V0.0.1 初次上传的版本,基本功能,但代码零散  

## 源码
https://gitee.com/fengssy/owork.connect