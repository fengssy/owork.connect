
/*
在nodejs环境下使用的一个实现例子
*/
var WebSocket = require('ws');


class ZWebSocket {
	url="";
	constructor(url, headers) {
		this.url = url;
		this.err = null;
		try {
			this.websocket = new WebSocket(url,{headers:headers});
		} catch (e) {
			this.err = e;
			console.log("new WebSocket出错了:", e);
		}
	}
	onOpen(callback) {
		this.websocket.onopen = callback;
	}
	onClose(callback) {
		this.websocket.onclose = function (closeEvent) {
			callback(closeEvent.code, closeEvent.reason);
		};
	}
	onError(callback) {
		this.websocket.onerror = function (event) {
			callback((event && event.message) ? event.message : "服务器走丢!");
		};
		//在注册错误事件前.就有错误了,直接触发一次回调
		if (this.err && callback) {
			var e = this.err;
			this.err = null;
			callback(e);
		}
	}
	onMessage(callback) {
		this.websocket.onmessage = function (messageEvent) {
			callback(messageEvent.data, null);
		};
	}
	send(data) {
		this.websocket.send(data);
	}
	close(code, reason) {
		//清理回调,防止触发
		this.websocket.onmessage = null;
		this.websocket.onerror = null;
		this.websocket.onclose = null;
		//执行关闭
		this.websocket.close(code || 1000, reason);
	}
}

//全局注册一下ZWebSocket
global.ZWebSocket=ZWebSocket;



if(typeof module!=="undefined"){
    module.exports=ZWebSocket;
}
