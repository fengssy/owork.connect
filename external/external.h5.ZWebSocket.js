
/*
在H5环境下使用的一个实现例子
*/

window.ZWebSocket = function (url, headers) {
	this.url = url;
	this.err = null;
	var queryArr = [];
	if (headers) {
		for (var key in headers) {
			queryArr.push(key + "+" + encodeURIComponent(headers[key]));
		}
	}
	//(统一使用Sec-WebSocket-Protocol头来存放实际的请求头键值对)
	//浏览器WebSocket构造第二个参数发给服务器就是用headers["Sec-WebSocket-Protocol"]接收,数组将被使用", "来分隔
	try {
		this.websocket = new WebSocket(url, queryArr);
	} catch (e) {
		this.err = e;
		console.log("new WebSocket出错了:", e);
	}
};
ZWebSocket.prototype.url = "";
ZWebSocket.prototype.onOpen = function (callback) {
	this.websocket.onopen = callback;
};
ZWebSocket.prototype.onClose = function (callback) {
	this.websocket.onclose = function (closeEvent) {
		callback(closeEvent.code, closeEvent.reason);
	};
};
ZWebSocket.prototype.onError = function (callback) {
	this.websocket.onerror = function (event) {
		callback((event && event.message) ? event.message : "服务器走丢!");
	};
	//在注册错误事件前.就有错误了,直接触发一次回调
	if (this.err && callback) {
		var e = this.err;
		this.err = null;
		callback(e);
	}
};
ZWebSocket.prototype.onMessage = function (callback) {
	this.websocket.onmessage = function (messageEvent) {
		var reader = new FileReader();
		reader.onload = function (e) {
			callback(e.target.result, null);
		};
		reader.readAsArrayBuffer(messageEvent.data);
	};
};
ZWebSocket.prototype.send = function (data) {
	this.websocket.send(data);
};
ZWebSocket.prototype.close = function (code, reason) {
	//清理回调,防止触发
	this.websocket.onmessage = null;
	this.websocket.onerror = null;
	this.websocket.onclose = null;
	//执行关闭
	this.websocket.close(code || 1000, reason);
};
