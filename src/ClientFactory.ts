import { Client } from "./Client";
import { ProtobufHelper } from "./ProtobufHelper";


/**
 * 客户端工厂,用来创建各种连接的客户端实现
 *
 * @export
 * @class ClientFactory
 */
export class ClientFactory {

    /**
     *生成websocket的创建工厂,需要全局环境定义 *ZWebSocket*
     *
     * @static
     * @param {string} url
     * @param {(object|null)} headers
     * @return {*}  {(()=>IChannel)}
     * @memberof ClientFactory
     */
    public static buildWebSocketFactory(url: string, headers: object | null): (() => IChannel) {
        return () => {
            return new ZWebSocket(url, headers);
        };
    }

    /**
     * 生成websocket的客户端,需要全局环境定义 *ZWebSocket*
     *
     * @static
     * @param {ProtobufHelper} protobufHelper 
     * @param {string} url
     * @param {(object|null)} headers
     * @return {*}  {Client}
     * @memberof ClientFactory
     */
    public static buildWebSocketClient(protobufHelper: ProtobufHelper, url: string, headers: object | null): Client {
        return new Client(protobufHelper, ClientFactory.buildWebSocketFactory(url, headers));
    }
}