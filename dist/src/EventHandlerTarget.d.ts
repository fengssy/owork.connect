/**
 *自身带有事件处理功能的类
 *
 * @export
 * @class EventHandlerTarget
 */
export declare class EventHandlerTarget {
    private eventHandlers;
    /**
     *注册事件
     *
     * @param {string} eventName 事件名
     * @param {Function} callback 回调
     * @param {object} target 回调执行指向对象
     * @memberof Client
     */
    on(eventName: string, callback: (...any: any[]) => void, target: object): void;
    /**
     * 注销事件
     *
     * @param {string} eventName 事件名
     * @param {Function} callback 回调
     * @param {object} target 回调执行指向对象
     * @memberof Client
     */
    off(eventName: string, callback: (...any: any[]) => void, target: object): void;
    /**
     * 触发事件
     *
     * @param {string} eventName 事件名
     * @param {any[]} eventParams 传给回调的参数
     * @memberof Client
     */
    emit(eventName: string, ...eventParams: any[]): void;
}
