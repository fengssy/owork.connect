/// <reference path="../../define/IChannel.d.ts" />
/// <reference types="long" />
import { EventHandlerTarget } from "./EventHandlerTarget";
import { ProtobufHelper } from "./ProtobufHelper";
/**
 * 将IMSession进一步封装为连接客户端,走事件
 *
 * @export
 * @class Client
 * @extends {EventHandlerTarget}
 */
export declare class Client extends EventHandlerTarget {
    private currChannelId;
    private rpcId;
    private resCallback;
    private resCallbackOpcode;
    private resCallbackTimeout;
    private protobufHelper;
    private connectChannelFactory;
    /**
     *连接对象
     *
     * @type {(Readonly<IChannel | null>)}
     * @memberof Client
     */
    channel: Readonly<IChannel | null>;
    /**
     *当前是否连接
     *
     * @type {boolean}
     * @memberof Client
     */
    connected: Readonly<boolean>;
    /**
     *当前是否连接并认证通过
     *
     * @type {boolean}
     * @memberof Client
     */
    authed: Readonly<boolean>;
    /** 强制断开连接并重置和清理相关数据 */
    private abortChannel;
    private onCloseProc;
    private onErrorProc;
    private onMessageProc;
    /**
     * Creates an instance of IMSession.
     * @param {ProtobufHelper} protobufHelper
     * @param {()=>IChannel} connectChannelFactory 开始连接并返回连接对象
     * @memberof IMSession
     */
    constructor(protobufHelper: ProtobufHelper, connectChannelFactory: () => IChannel);
    /**
     *连接服务器,并由外部完成认证,要求回传认证结果
     *
     * @param {(errMsg: string, authCallback: (connected: boolean) => void) => void} callback 完成连接,成功则errMsg=null,并且要求回传认证结果(authCallback),失败则只有errMsg
     * @memberof Client
     */
    connect(callback: (errMsg: string | null, authCallback: (authed: boolean) => void) => void): void;
    /**
     *断开连接,并且不触发事件
     *
     * @memberof Client
     */
    disconnect(): void;
    /**
     * 发送协议Rpc消息,并返回是否发送成功,当前连接已经断开或者消息序列化失败等都会发送失败
     *
     * @param {number} opcode 消息类型码
     * @param {*} msg 消息对象,需要对应的protobuf类型实例
     * @param {number|null} replyOpcode 回复消息的类型码
     * @param {(((connectErr:string,reOpCode: number, msg: ResultT|null) => void) | null)} [onReply=null] 当消息需要回复时必传 (connectErr为通讯错误,如"timeout","cancel")
     * @param {number} rpcWaitTimeoutMS 如果是rpc消息,则可设置等待的超时毫秒数,超时则触发onReply("timeout", 0, null)
     * @memberof Session
     */
    sendRpc<RequestT extends {
        RpcId?: number | Long | null;
    }, ResponseT extends {
        RpcId?: number | Long | null;
        Error?: number | Long | null;
        Message?: string | null;
    }>(opcode: number, msg: RequestT, replyOpcode?: number | null, onReply?: ((connectErr: string | null, reOpCode: number, msg: ResponseT) => void) | null, rpcWaitTimeoutMS?: number): boolean;
    /**
     *发送不需要回调的协议消息,并返回是否发送成功,当前连接已经断开或者消息序列化失败等都会发送失败
     *
     * @param {number} msgTypeCode 消息类型码
     * @param {*} msg 消息对象,需要对应的protobuf类型实例
     * @memberof Client
     */
    sendMsg(msgTypeCode: number, msg: any): boolean;
    /**
     *自动重发Rpc消息,3秒没回调将会自动重发!
     *
     * @template MsgT
     * @template ResultT
     * @param {number} opcode
     * @param {MsgT} req
     * @param {number} resultOpcode
     * @param {(((errMsg: string | null, result: ResultT | null) => void) | null)} callback
     * @param {(msg:string)=>void} showLoading 需要显示加载中时会触发
     * @param {()=>void} hideLoading 需要隐藏加载中时会触发
     * @return {*}  {void}
     * @memberof Client
     */
    sendRpcAutoRetry<MsgT extends {
        RpcId?: number;
    }, ResultT extends {
        RpcId?: number;
        Error?: number;
        Message?: string;
    }>(opcode: number, req: MsgT, resultOpcode: number, callback: ((errMsg: string | null, result: ResultT | null) => void) | null, showLoading: (msg: string) => void, hideLoading: () => void): void;
    /**
     *当连接被关闭时触发,只在完成连接(认证成功)后触发,并且手动断开连接不触发
     *
     * @param {(client: Client, code: number, reason: string) => void} callback 如果是连接异常关闭则code=1,reason为具体错误消息
     * @param {object} target
     * @memberof Client
     */
    onClose(callback: (client: Client, code: number, reason: string) => void, target: object): void;
    offClose(callback: (client: Client, code: number, reason: string) => void, target: object): void;
    private triggerClose;
    /**
     *当收到消息时触发
     *
     * @param {((client: Client, opcode: number, msg: any, reply: ((resultOpcode: number, result: any) => void) | null, remoteInfo: any) => void)} callback 如果收到的是需要响应的消息,需要自行调用reply进行回复,remoteInfo为udp类的协议时使用
     * @param {object} target
     * @memberof Client
     */
    onMessage(callback: (client: Client, opcode: number, msg: any, reply: ((resultOpcode: number, result: any) => void) | null, remoteInfo: any) => void, target: object): void;
    offMessage(callback: (client: Client, opcode: number, msg: any, reply: ((resultOpcode: number, result: any) => void) | null, remoteInfo: any) => void, target: object): void;
    private triggerMessage;
}
