"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventHandlerTarget = void 0;
/**
 *自身带有事件处理功能的类
 *
 * @export
 * @class EventHandlerTarget
 */
var EventHandlerTarget = /** @class */ (function () {
    function EventHandlerTarget() {
        this.eventHandlers = new Map();
    }
    /**
     *注册事件
     *
     * @param {string} eventName 事件名
     * @param {Function} callback 回调
     * @param {object} target 回调执行指向对象
     * @memberof Client
     */
    EventHandlerTarget.prototype.on = function (eventName, callback, target) {
        var fns = this.eventHandlers.get(eventName);
        if (!fns)
            fns = new Set();
        fns.add({
            thisTarget: target,
            handler: callback
        });
        this.eventHandlers.set(eventName, fns);
    };
    /**
     * 注销事件
     *
     * @param {string} eventName 事件名
     * @param {Function} callback 回调
     * @param {object} target 回调执行指向对象
     * @memberof Client
     */
    EventHandlerTarget.prototype.off = function (eventName, callback, target) {
        var fns = this.eventHandlers.get(eventName);
        if (fns) {
            fns.forEach(function (item) {
                if (item.handler === callback && item.thisTarget === target) {
                    fns.delete(item);
                }
            });
        }
    };
    /**
     * 触发事件
     *
     * @param {string} eventName 事件名
     * @param {any[]} eventParams 传给回调的参数
     * @memberof Client
     */
    EventHandlerTarget.prototype.emit = function (eventName) {
        var eventParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            eventParams[_i - 1] = arguments[_i];
        }
        var fns = this.eventHandlers.get(eventName);
        if (fns) {
            fns.forEach(function (item) {
                item.handler.apply(item.thisTarget, eventParams);
            });
        }
    };
    return EventHandlerTarget;
}());
exports.EventHandlerTarget = EventHandlerTarget;
