import { Client } from "./Client";
import { ProtobufHelper } from "./ProtobufHelper";
/**
 * 客户端工厂,用来创建各种连接的客户端实现
 *
 * @export
 * @class ClientFactory
 */
export declare class ClientFactory {
    /**
     *生成websocket的创建工厂,需要全局环境定义 *ZWebSocket*
     *
     * @static
     * @param {string} url
     * @param {(object|null)} headers
     * @return {*}  {(()=>IChannel)}
     * @memberof ClientFactory
     */
    static buildWebSocketFactory(url: string, headers: object | null): (() => IChannel);
    /**
     * 生成websocket的客户端,需要全局环境定义 *ZWebSocket*
     *
     * @static
     * @param {ProtobufHelper} protobufHelper
     * @param {string} url
     * @param {(object|null)} headers
     * @return {*}  {Client}
     * @memberof ClientFactory
     */
    static buildWebSocketClient(protobufHelper: ProtobufHelper, url: string, headers: object | null): Client;
}
