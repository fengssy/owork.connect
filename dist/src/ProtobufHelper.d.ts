/// <reference path="../../define/ProtobufHelper.d.ts" />
/**
 * protobuf的工具类,实例化使用
 * 建立在数据传输协议基础上的功能封装,如消息类型码映射成proto里的具体类型等
 */
export declare class ProtobufHelper {
    private anyTypeUrl;
    private util;
    private pRoot;
    private lvNS;
    private opcodeToName;
    /**
     * Creates an instance of ProtobufHelper.
     * @param {any} root 没注册到全局则需要自行获取传入; 全局则传null,会自动取$protobufjs.roots["default"]
     * @param {string} msgPackageName proto里的包名
     * @param {(op: number) => string} opcodeToName 将类型码映射为类名
     * @param {(any | null)} [typeRegGlobal=globalThis] msgPackageName将被注册到这个对象下,方便枚举可以直接无缝使用,不传使用globalThis(web环境用window,node环境用global),传null则不注册
     * @memberof ProtobufHelper
     */
    constructor(root: any, msgPackageName: string, opcodeToName: (op: number) => string, typeRegGlobal?: any | null);
    private getMsgProto;
    /**
     *根据类型码获取消息类名
     *
     * @param {number} op
     * @return {*}  {(string | null)}
     * @memberof ProtobufHelper
     */
    getMsgName(op: number): string | null;
    /**
     *根据消息类名获取类型
     *
     * @template T
     * @param {string} className
     * @return {*}  {(T | null)}
     * @memberof ProtobufHelper
     */
    getClass<T>(className: string): T | null;
    /**
     *将类型码和消息体编码成二进制数组
     *
     * @param {number} op
     * @param {object} msg
     * @return {*}  {(Uint8Array|null)}
     * @memberof ProtobufHelper
     */
    encode(op: number, msg: any): Uint8Array | null;
    /**
     *根据类型码和消息二进制,解码成消息对象
     *
     * @param {number} op
     * @param {Uint8Array} buf
     * @return {*}  {(object| null)}
     * @memberof ProtobufHelper
     */
    decode(op: number, buf: Uint8Array): any | null;
    /**
     *将any类型解包出消息对象,解析失败返回null (root中必须包含google.protobuf.Any对象,即导入官方的Any.proto文件)
     *
     * @param {*} anyObj
     * @return {*}  {(object | null)}
     * @memberof ProtobufHelper
     */
    anyUnpack(anyObj: google.protobuf.IAny): AnyUnpackResult | null;
    /**
     *将消息对象打包成google.protobuf.Any类型 (root中必须包含google.protobuf.Any对象,即导入官方的Any.proto文件)
     *
     * @param {*} anyObj
     * @param {string} typeFullName
     * @return {google.protobuf.Any}
     * @memberof ProtobufHelper
     */
    anyPack(anyObj: any, typeFullName: string): google.protobuf.IAny | null;
}
