"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientFactory = void 0;
var Client_1 = require("./Client");
/**
 * 客户端工厂,用来创建各种连接的客户端实现
 *
 * @export
 * @class ClientFactory
 */
var ClientFactory = /** @class */ (function () {
    function ClientFactory() {
    }
    /**
     *生成websocket的创建工厂,需要全局环境定义 *ZWebSocket*
     *
     * @static
     * @param {string} url
     * @param {(object|null)} headers
     * @return {*}  {(()=>IChannel)}
     * @memberof ClientFactory
     */
    ClientFactory.buildWebSocketFactory = function (url, headers) {
        return function () {
            return new ZWebSocket(url, headers);
        };
    };
    /**
     * 生成websocket的客户端,需要全局环境定义 *ZWebSocket*
     *
     * @static
     * @param {ProtobufHelper} protobufHelper
     * @param {string} url
     * @param {(object|null)} headers
     * @return {*}  {Client}
     * @memberof ClientFactory
     */
    ClientFactory.buildWebSocketClient = function (protobufHelper, url, headers) {
        return new Client_1.Client(protobufHelper, ClientFactory.buildWebSocketFactory(url, headers));
    };
    return ClientFactory;
}());
exports.ClientFactory = ClientFactory;
