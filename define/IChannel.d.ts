
/**
 * 定义基础通讯连接的接口
 *
 * @interface IChannel
 */
declare interface IChannel {
    /**
     *连接成功时触发
     *
     * @param {(ev: object) => any} callback
     * @memberof IChannel
     */
    onOpen(callback: (ev: object) => any): void;
    /**
     *连接被关闭时触发
     *
     * @param {(code: number, reason: string) => any} callback
     * @memberof IChannel
     */
    onClose(callback: (code: number, reason: string) => any): void;
    /**
     *连接出错时触发
     *
     * @param {(errMsg: string) => any} callback
     * @memberof IChannel
     */
    onError(callback: (errMsg: string) => any): void;
    /**
     *收到网络流数据呢
     *
     * @param {(data: ArrayBuffer, remoteInfo:any) => any} callback remoteInfo为udp协议类的连接时使用
     * @memberof IChannel
     */
    onMessage(callback: (data: ArrayBuffer, remoteInfo:any) => any): void;

    /**
     *发送二进制数据,写入网络流
     *
     * @param {ArrayBuffer} data
     * @memberof IChannel
     */
    send(data: ArrayBuffer):void;
    /**
     *关闭连接
     *
     * @param {number} code
     * @param {(string|null)} reason
     * @memberof IChannel
     */
    close(code: number, reason: string|null):void;
}