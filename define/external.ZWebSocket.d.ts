
//本定义文件为需要外部实现的全局类/函数


declare class ZWebSocket implements IChannel{
    url: string;
    constructor(url:string,headers:object|null);
    onOpen(callback: (ev: object) => any): void;
    onClose(callback: (code: number, reason: string) => any): void;
    onError(callback: (errMsg: string) => any): void;
    onMessage(callback: (data: ArrayBuffer, remoteInfo:any) => any): void;

    send(data: ArrayBuffer):void;
    close(code: number, reason: string|null):void;
}

