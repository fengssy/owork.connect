
/**
 *解析出google.protobuf.Any类型的结果
 *
 * @interface AnyUnpackResult
 */
 declare interface AnyUnpackResult {
    /**
     *里面包含的原始对象
     *
     * @type {*}
     * @memberof AnyUnpackResult
     */
    unpackObject: any;
    /**
     *这个对象的完整类型名(如果有定义则还包含了包名)
     *
     * @type {string}
     * @memberof AnyUnpackResult
     */
    typeName: string;
}

/** Namespace google. */
declare namespace google {
    /** Namespace protobuf. */
    namespace protobuf {
        /** Properties of an Any. */
        interface IAny {
            /** Any typeUrl */
            typeUrl?: (string|null);
            /** Any value */
            value?: (Uint8Array|null);
        }
    }
}
